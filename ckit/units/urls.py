from django.urls import path

from .views import UnitCreateView, UnitDeleteView, UnitsView

app_name = "units"

urlpatterns = [
    path("", UnitsView.as_view(), name="list"),
    path("new/", UnitCreateView.as_view(), name="new"),
    path("rm/<str:pk>/", UnitDeleteView.as_view(), name="remove"),

]
