from django.test import TestCase

from .models import Unit


class AnimalTestCase(TestCase):
    fixtures = ["units.yaml"]

    def setUp(self):
        # Test definitions as before.
        pass

    def test_unit_model_is_empty(self):
        self.assertTrue(Unit.objects.all(), "Unit model it empty!")
