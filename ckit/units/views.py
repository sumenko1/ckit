from typing import Any, Dict

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, DeleteView

from .forms import UnitForm
from .models import Unit


class UnitCreateView(LoginRequiredMixin, CreateView):
    model = Unit
    form_class = UnitForm
    success_url = reverse_lazy("units:view")
    login_url = reverse_lazy("login")


class UnitsView(TemplateView):
    template_name = "units/view_units.html"

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        units = Unit.objects.all()
        context["table"] = {
            # "counter": True,
            "heads": ("Наименование", '#action'),
            "lines": [
                {
                    "data": [unit.name],
                    # "link": reverse_lazy('projects:detail', kwargs={'pk': project.pk}),
                    "delete_link": reverse_lazy(
                       "units:remove", kwargs={"pk": unit.pk}
                    ),
                }
                for unit in units
            ],
        }
        return context

class UnitDeleteView(LoginRequiredMixin, DeleteView):
    model = Unit
    success_url = reverse_lazy("units:list")

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["back_url"] = reverse_lazy("units:list")
        return context
    
    def form_valid(self, request):
        self.object = self.get_object()
        if self.request.user.is_superuser:
            success_url = self.get_success_url()
            self.object.delete()
            return HttpResponseRedirect(success_url)
        return render(self.request, 'units/unit_delete_forbidden.html',
                      context={'back_url': self.get_success_url()})
