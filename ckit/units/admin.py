from django.contrib import admin

from .models import Unit


class UnitsAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "name",
    )


admin.site.register(Unit, UnitsAdmin)
