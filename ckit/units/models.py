from django.db import models
from templates.messages import messages


class Unit(models.Model):
    name = models.CharField(
        verbose_name="Ед.изм.",
        unique=True,
        blank=False,
        max_length=15,
        error_messages={"unique": messages["unique"]},
        primary_key=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Единицы измерения"
        verbose_name_plural = verbose_name
