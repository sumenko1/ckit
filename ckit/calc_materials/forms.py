from django.forms import CharField, HiddenInput, ModelForm, TextInput, NumberInput
from .models import ProjectTree


class EditTreeNode(ModelForm):
    val = TextInput()
    class Meta:
        model = ProjectTree
        fields = '__all__'
        widgets = {
            'parent': HiddenInput(),
            'element': TextInput(attrs={'size': 5, 'title': '!!'}),
            'qty': NumberInput(attrs={'autofocus': 'autofocus'})
        }
    # element.widget.attrs.update(size="20")
