from django.apps import AppConfig


class CalcMaterialsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "calc_materials"
