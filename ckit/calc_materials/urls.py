from django.urls import path

from .views import (
    add_to_tree, delete_from_tree, short_tree, get_update_node, get_tree
)

app_name = "calc_materials"

urlpatterns = [
    path("<int:pk>/", short_tree, name="list"),
    path("<int:pk>/add/<int:node>/<int:pk_element>/", add_to_tree, name="add"),
    path("<int:pk>/rm/<int:node>/", delete_from_tree, name="remove"),
    path("<int:pk>/node/<int:node_pk>/", get_update_node, name="edit_node"),
    path("<int:pk>/g/", get_tree, name="edit_node"),
]
