from decimal import ROUND_UP, Decimal
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.http import HttpResponseNotFound
from django.contrib.auth import get_user_model
from django.db.models import Q, Sum
from django.shortcuts import HttpResponse, get_object_or_404, render
from django.urls import reverse_lazy
from mptt.templatetags.mptt_tags import cache_tree_children

from materials.models import Material, ProjectHasMaterial
from projects.models import Project

from .models import CalcTable, ProjectTree
from materials.models import MaterialGroup
import json


User = get_user_model()


def recursive_node_to_dict(node):
    material = node.element.material.name
    qty = node.qty
    units = node.element.material.units
    
    result = {
        'id': node.pk,
        'text': f'{material} <span class="badge bg-secondary"><small>{qty:.1f} {units}</small></span>',
        # 'state': {'opened': True}
    }
    if node.element.level < 3:
        result['state'] = {'opened': True}
    if node.is_leaf_node() and node.element.level == 5:
            result["icon"] = "/static/bolt_9023203_16.png"
    if node.is_leaf_node() and node.element.level < 5:
             result["icon"] = "/static/link-solid.svg"

    children = [recursive_node_to_dict(c) for c in node.get_children()]
    if children:
        result['children'] = children
    return result

@login_required
def get_tree(request, pk):
    tree = ProjectTree.objects.filter(element__project__id=pk).select_related('element')
    root_nodes = cache_tree_children(tree)
    dicts = []
    is_ajax_request = True
    does_req_accept_json = request.accepts("application/json")
    is_ajax_request = request.headers.get('x-requested-with') == "XMLHttpRequest" and does_req_accept_json
    html = ''
    if not is_ajax_request:
        return HttpResponseNotFound('error')
    
    for n in root_nodes:
        dicts.append(recursive_node_to_dict(n))

    js = json.dumps(dicts, indent=2)
    return HttpResponse(js, status=200)


@login_required
def short_tree(request, pk):
    active_node = request.GET.get('active', None)
    if not active_node:
        qs = ProjectTree.objects.filter(parent=None, element__project__pk=pk)
        if qs:
            active_node = qs[0].pk


    if active_node and not ProjectTree.objects.filter(pk=active_node).exists():
        active_node = ProjectTree.objects.get(parent=None,
                                              element__project__pk=pk).pk
    tree = ProjectTree.objects.filter(element__project__pk=pk)
    root = tree.get(parent=None)

    title = get_object_or_404(Project, pk=pk).name
    elements = ProjectHasMaterial.objects.filter(
        project=pk).order_by('-level', 'material__name')
    list_add = [
            {
                'value': element.pk,
                'name': element.material.name,
                'is_disabled': False}
            for element in elements
        ]
    user = User.objects.get(username=request.user)
    user_materials = Material.objects.filter(
        Q(owner=user) | Q(owner__is_staff=True)).order_by('name')
    
    does_req_accept_json = request.accepts("application/json")
    is_ajax_request = (request.headers.get('x-requested-with') == "XMLHttpRequest" and does_req_accept_json)

    if is_ajax_request:
        table = calculate(root)
        html = render_to_string(
            template_name='includes/widgets/table.html',
            context={'table': table}
        )
        return HttpResponse(html)
    table = calculate(root)

    groups = MaterialGroup.objects.all()
    groups = groups.filter(Q(owner=request.user) | Q(owner__is_staff=True) & Q(pk__in=[p.material.pk for p in elements]))

    return render(request, 'calc_materials/short_tree.html', 
                  context={
                   'active_node': active_node,
                   'tree': tree,
                   'action_name': 'Добавить',
                   'action_url': False,
                   'back_url': reverse_lazy('projects:detail',
                                            kwargs={'pk': pk}),
                    'pk': pk, 'title': title,
                    'list_add': list_add,
                    'user_materials': user_materials,
                    'table': table,
                    'groups': groups,
                    })


@login_required
def add_to_tree(request, pk, node, pk_element):
    """ Add material from library to tree """
    print(':', pk, node, pk_element)
    if not Project.objects.filter(pk=pk, owner=request.user).exists():
        print(1)
        return HttpResponse('you can access your own projects only', status=400)
    
    if not ProjectHasMaterial.objects.filter(project=pk, pk=pk_element).exists():
        print(2)
        return HttpResponse('project=pk material=pk_element', status=400)
    
    if not ProjectTree.objects.filter(pk=node, element__project__pk=pk).exists():
        print(3)
        return HttpResponse('node not in project tree', status=400)
    
    if ProjectTree.objects.filter(parent__element__project__pk=pk,
                                  parent__pk=node,
                                  element__pk=pk_element).exists():
        print(4)
        return HttpResponse('node already has pk_element', status=400)
    
    if node == pk_element:
        print(5)
        return HttpResponse('material == pk_element', status=400)
    
    node_obj = ProjectTree.objects.get(pk=node)
    
    parent_level = ProjectTree.objects.get(pk=node).element.level
    child_level = ProjectHasMaterial.objects.get(pk=pk_element).level

    if child_level <= parent_level:
        print(7)
        return HttpResponse(f'Wrong levels. Parent: {parent_level} child: {child_level}',
                            status=400)


    children_list = [n.element.pk for n in node_obj.get_ancestors(
        include_self=True)]
    
    if pk_element in children_list:
        print(8)
        return HttpResponse('this node cannot be added, it is a parent node',
                            status=400)

    q1 = ProjectTree.objects.filter(
                                    parent__element__project__pk=pk,
                                    parent__element__pk=node_obj.element.pk
                                    ).exclude(parent__pk=node).count()
    
    if q1:
        print(9)
        return HttpResponse('this node already defined', status=400)

    element = ProjectHasMaterial.objects.get(project=pk, pk=pk_element)
    ProjectTree.objects.create(parent=node_obj, element=element, qty=Decimal(1))
    return HttpResponse(f'!add {pk_element} to {pk}.{node}')


@login_required
def delete_from_tree(request, pk, node):
    if not Project.objects.filter(pk=pk, owner=request.user).exists():
        return HttpResponse('you can access your own projects only', status=400)
    if not ProjectTree.objects.filter(pk=node, element__project__pk=pk).exists():
        return HttpResponse('node not in project tree', status=400)
    
    obj = ProjectTree.objects.filter(pk=node, element__project__pk=pk).prefetch_related('element', 'element__material')
    if obj[0].element.level == 0:
        return HttpResponse('unable delete root node', status=400)

    ProjectTree.objects.get(pk=node, element__project__pk=pk).delete()
    return HttpResponse('delete')


def calculate(root):
    task = {}
    task_list = []
    project = root.element.project

    for t in root.get_family():
        ancestors = t.get_ancestors(include_self=True)
        qty = 1
        for obj in ancestors:
            qty = qty * obj.qty
        element = ancestors.last()
        if element.parent:
            material = element.element
        else:
            material = root.element

        task_list.append(CalcTable(element=element, qty=qty, project=project))        
        try:
            task[material] += qty
        except KeyError:
            task[material] = qty

    CalcTable.objects.filter(project=project).delete()
    CalcTable.objects.bulk_create(task_list)

    table_data = CalcTable.objects.filter(project=project).values(
        'element__element__material__name',
        'element__element__level',
        'element__element__material__units'
        ).annotate(Sum('qty')).order_by('-element__element__level',
                                        'element__element__material__name')
    
    clean_data = table_data.filter(Q(element__element__level=ProjectHasMaterial.Levels.LEAF) and Q(element__element__add_to_report=True))
    
    table = {
        'heads': ['Наименование', 'Ед.изм.', 'Кол.'],
        'lines': [
                {'data': [k['element__element__material__name'],
                          k['element__element__material__units'],
                          k['qty__sum'].quantize(Decimal('0.1'), ROUND_UP)] } for k in clean_data
                ]

    }
    return table


@login_required
@require_http_methods(['GET', 'POST'])
def get_update_node(request, pk, node_pk):
    node = get_object_or_404(ProjectTree, pk=node_pk, element__project__pk=pk)
    
    if request.POST:
        qty = Decimal(request.POST.get('qty', 0))
        
        node.qty = qty
        node.save()
        return HttpResponse(f'{pk} {node_pk}', status=201)
    
    html = render_to_string(template_name='calc_materials/widgets/edit_node.html',
                                context={'node': node})
    return HttpResponse(html)

