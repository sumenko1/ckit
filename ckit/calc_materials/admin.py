from calc_materials.models import CalcTable, ProjectTree
from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin, MPTTModelAdmin


class CalcTableAdmin(admin.ModelAdmin):
    list_display = (
        "project",
        "element",
        "qty"
    )

admin.site.register(ProjectTree, DraggableMPTTAdmin)
admin.site.register(CalcTable, CalcTableAdmin)
