var actionElement = 0;

$(function () {
    // not working yet
    $('#jstree').jstree(
      {
        'core' : {
            'check_callback' : function (operation, node, node_parent, node_position, more) {
                // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node', 'copy_node' or 'edit'
                // in case of 'rename_node' node_position is filled with the new node name
                console.debug(operation + " node " + node + " node_parent " + node_parent + " node_position " + "more" + more)
                return operation === 'rename_node' ? true : false;
            }
        },
        
        "plugins": ['state', 'wholerow'],
        "state" : {'key': 'a1s2d3f5g'},
        })
    $('#jstree').on("changed.jstree", function (e, data) {

        var xhr = new XMLHttpRequest();
        actionElement = data.selected[0];
        if(!isNaN(actionElement))
        {
            $('#event_result').html('!Selected: ' + actionElement);
    
            xhr.open('GET', 'node/' + actionElement + '/');
            
            xhr.onreadystatechange = function () {
                if(xhr.readyState === 4 && xhr.status === 200) {
                    document.getElementById('node_info').innerHTML = xhr.responseText;
                }
            }
            xhr.send();
        }
      })
})


function addItemToTree (id) {
    var xhr = new XMLHttpRequest();

    if(id !== undefined & actionElement!=NaN){
        xhr.open('GET', 'add/' + actionElement + '/' + id);
        xhr.onreadystatechange = function () {
            if(xhr.readyState === 4 && xhr.status === 200) {
                loadPageData();
            }
        };
        xhr.send();
    } else {
        alert('Nothing selected');
    }
}

// add element to tree
$(function () {
    $('#addButton').on('click', function () {
        const si = $('#list').find(":selected").val();
        var xhr = new XMLHttpRequest();

        if(si !== undefined & actionElement!=NaN){
            xhr.open('GET', 'add/' + actionElement + '/' + si);
            xhr.onreadystatechange = function () {
                if(xhr.readyState === 4 && xhr.status === 200) {
                    loadPageData();
                }
            };
            xhr.send();
        } else {
            alert('Nothing selected');
        }

    })
})

// delete element from tree
$(function () {
    $('#deleteButton').on('click', function deleteElement() {
        var xhr = new XMLHttpRequest();
        console.debug('actionElement: ' + actionElement);
        if(actionElement === undefined) alert('Nothing selected')
        
        if(actionElement){
            xhr.open('GET', 'rm/' + actionElement + '/');
            
            xhr.onreadystatechange = function () {

                if(xhr.readyState === 4 && xhr.status === 400){
                    alert('error ' + xhr.status + ' : ' + xhr.responseText)
                    document.getElementById('response').innerHTML = 'error ' + xhr.status + ': ' + xhr.responseText;
                    return;
                }
                if(xhr.readyState === 4 && xhr.status === 200) {
                    document.getElementById('response').innerHTML = xhr.responseText;
                    loadPageData();
                }
            };
            xhr.send();
        } else {
            console.debug('[delete] nothing selected');
        }
    })
})

// modify qty for node
function saveQty() {
    const elem = parseFloat(document.getElementById('id_qty').value);
    console.debug('set ' + actionElement + ' to ' + elem);

    if(!isNaN(elem)){
        const data = new FormData();
        data.append('qty', elem);
        const request = new XMLHttpRequest();
        request.open('POST', 'node/' + actionElement + '/');
        request.setRequestHeader("X-CSRFToken", document.querySelector('[name=csrfmiddlewaretoken]').value);
        request.onreadystatechange = function () {
            if(request.readyState === 4 && request.status === 201)
                {
                    loadPageData();
                }
        };
        request.send(data);
    } else {
        console.debug('check-error');
    }

}
$(function () {
    $("#save_qty").on("click", saveQty)
})
