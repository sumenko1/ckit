from decimal import ROUND_UP, Decimal

from django.core.exceptions import ValidationError
from django.db import models
from materials.models import ProjectHasMaterial
from mptt.models import MPTTModel, TreeForeignKey
from projects.models import Project


class ProjectTree(MPTTModel):
    parent = TreeForeignKey(
        "self",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="children",
        verbose_name="Узел",
    )
    element = models.ForeignKey(
        ProjectHasMaterial,
        on_delete=models.CASCADE,
        verbose_name="Элемент",
        related_name="nodes",
    )
    qty = models.DecimalField(
        max_digits=11,
        decimal_places=4,
        verbose_name="Количество",
        null=False,
        blank=False,
        default=Decimal("1.0"),
    )

    def __str__(self):
        if not self.parent:
            return (f"{self.element.project.owner}.{self.element.project}: "
                    f"{self.element} [ {self.qty:0.0} ]")
        qty = self.qty.quantize(Decimal(".1"), rounding=ROUND_UP)
        return f"{self.element.material.name}: [ {qty} ]"

    def clean(self):
        if self.parent and self.parent.element == self.element:
            raise ValidationError("Нельзя добавить элемент в себя")

        qs = ProjectTree.objects.filter(
            parent=self.parent, element=self.element
        )
        if self.pk is not None:
            qs = qs.exclude(pk=self.pk)
        if qs.exists() & self.element.add_to_report:
            raise ValidationError(
                "Такой элемент уже добавлен." " Добавьте другой."
            )
        if self.parent and self.parent.element.project != self.element.project:
            raise ValidationError("Можно добавить только элемент проекта")

    class Meta:
        verbose_name = "1 Дерево проекта"
        verbose_name_plural = verbose_name


class CalcTable(models.Model):
    element = models.ForeignKey(ProjectTree, on_delete=models.CASCADE)
    qty = models.DecimalField(
        max_digits=11,
        decimal_places=4,
        verbose_name="Количество",
        null=False,
        blank=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
