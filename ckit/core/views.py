from typing import Any, Dict

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.generic.base import TemplateView


class IndexView(TemplateView):
    template_name = "core/index.html"

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["links"] = (
            {"link": "projects:list", "name": "Проекты"},
            {"link": "units:list", "name": "Единицы измерения"},
            {"link": "materials:list", "name": "Материалы"},
            {"link": "materials:by_group", "name": "Материалы/группы"},
            {"link": "geo:list", "name": "Города"},
        )
        return context


def page_not_found(request, exception):
    # Переменная exception содержит отладочную информацию,
    # выводить её в шаблон пользователской страницы 404 мы не станем
    return render(
        request, "404.html", {"path": request.path}, status=404
    )


def server_error(request):
    return render(request, "500.html", status=500)
