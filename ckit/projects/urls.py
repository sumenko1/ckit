from django.urls import path

from .views import (
    ProjectCalculatorsView, ProjectDeleteView, ProjectsListView,
    add_create_material_to_project, add_update_material_to_project,
    new_update_project, project_view_materials, remove_material_from_project,
)

app_name = 'projects'

urlpatterns = [
    path('', ProjectsListView.as_view(), name='list'),
    path('new/', new_update_project, name='new'),
    path('edit/<int:pk>/', new_update_project, name='edit'),
    path('<int:pk>/', ProjectCalculatorsView.as_view(), name='detail'),
    path('<int:pk>/materials/', project_view_materials, name='project_materials'),
    path('<int:pk>/add/', add_update_material_to_project,
         name='add_to_project'),
    path('<int:pk>/add_create/', add_create_material_to_project,
         name='add_create_to_project'),
    path('<int:pk>/edit/<int:material_pk>/', add_update_material_to_project,
                                             name='mat-detail'),
    path(
        '<int:pk>/rm/<int:material_pk>',
        remove_material_from_project,
        name='remove_material',
    ),
    path('rm/<int:pk>/', ProjectDeleteView.as_view(), name='remove'),
]
