from calc_materials.models import ProjectTree
from django.core.exceptions import NON_FIELD_ERRORS
from django.db import transaction
from django.forms import CharField, HiddenInput, ModelForm, TextInput
from materials.models import Material, Project, ProjectHasMaterial
from templates.messages import messages

from .models import Project


class AddMaterialToProjectForm(ModelForm):
    
    class Meta:
        model = ProjectHasMaterial
        fields = ['material', 'level', 'project', 'add_to_report']
        widgets = {'project': HiddenInput()}


class AddProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = '__all__'
        widgets = {
            'owner': HiddenInput(),
            'name': TextInput(attrs={'autofocus': 'autofocus'}),
        }

        error_messages = {
            NON_FIELD_ERRORS: {'unique_together': f'Уже существует'}
        }


    @transaction.atomic
    def save(self, commit=True):
        instance = super(AddProjectForm, self).save(commit=False)
        if commit:
            instance.save()
            entry_material = Material.objects.get(name='/')
            entry = ProjectHasMaterial.objects.create(
                project=instance,
                material=entry_material,
                add_to_report=False,
                level=ProjectHasMaterial.Levels.ROOT
            )
            ProjectTree.objects.create(parent=None, element=entry, qty=1)
        return instance
