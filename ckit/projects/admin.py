from django.contrib import admin

from .models import Project


class ProjectsAdmin(admin.ModelAdmin):
    list_display = ("name", 'city',"owner")


admin.site.register(Project, ProjectsAdmin)
