from django.contrib.auth import get_user_model
from django.db import models
from geo.models import City
from templates.messages import messages

User = get_user_model()


class Project(models.Model):
    name = models.CharField(
        max_length=50, blank=False, verbose_name="Название проекта"
    )

    city = models.ForeignKey(City, null=True, blank=True,
                             verbose_name='Местоположение',
                             on_delete=models.DO_NOTHING)
    
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="projects",
        verbose_name="Владелец",
    )

    def __str__(self):
        return self.name


    class Meta:
        unique_together = [["name", "owner"]]
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"

    # city
    # address
    # buildings
    # properties
