from django.template.loader import render_to_string
from typing import Any, Dict
from django.db.models import Q


from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, DeleteView
from materials.models import ProjectHasMaterial, Material

from .forms import AddMaterialToProjectForm, AddProjectForm
from .models import Project
from materials.models import ProjectHasMaterial
from calc_materials.models import ProjectTree


User = get_user_model()


class ProjectsListView(LoginRequiredMixin, TemplateView):
    template_name = 'projects/view_project_list.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        projects = Project.objects.filter(owner=self.request.user)
        context['projects'] = projects
        context['table'] = {
            'counter': True,
            'heads': ('id', 'Проект', 'Место', '#action'),
            'lines': [
                {
                    'data': [project.pk, project.name,
                             project.city],
                    'link': reverse_lazy('projects:detail',
                                         kwargs={'pk': project.pk}),
                    'delete_link': reverse_lazy(
                       'projects:remove', kwargs={'pk': project.pk}
                    ),
                    'edit_link': reverse_lazy('projects:edit',
                                              kwargs={'pk': project.pk})
                }
                for project in projects
            ],
        }
        
        return context


@login_required
def new_update_project(request, pk=None):
    instance = None
    commit = True
    if pk:
        instance = get_object_or_404(Project, pk=pk, owner=request.user)
    else:
        instance = Project(owner=request.user)
    
    form = AddProjectForm(request.POST or None, instance=instance)
    if request.GET or not form.is_valid():
        return render(request, 'projects/project_form.html',
                      context={'form': form, 'pk': pk,
                               'back_url': reverse_lazy('projects:list')})
    
    if pk: # if update
        commit=False
    a = form.save(commit=commit)
    a.save()
    return redirect('projects:list')


class ProjectNewView(LoginRequiredMixin, CreateView):
    form_class = AddProjectForm
    template_name = 'projects/project_form.html'
    success_url = reverse_lazy('projects:list')
    login_url = reverse_lazy('login')

    def get_form_kwargs(self) -> Dict[str, Any]:
        # Put user to kw for form class
        kwargs = super(ProjectNewView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class ProjectCalculatorsView(LoginRequiredMixin, TemplateView):
    template_name = 'projects/view_project_detail.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['calculators'] = [
            {
                'name': 'Подсчет материалов',
                'link': 'calc_materials:list'},
            {
                'name': 'Библиотека материалов',
                'link': 'projects:project_materials',
            },
            {'name': 'Расчет кровли', 'link': ''},
            {'name': 'Расчет балок и стропил', 'link': ''},
        ]
        pk = kwargs['pk']
        context['pk'] = pk
        context['title'] = get_object_or_404(Project, pk=pk).name
        return context


@login_required
def add_update_material_to_project(request, pk, material_pk=None):
    """ Add material to project library """
    project = Project.objects.get(pk=pk)
    if request.user != project.owner:
        return redirect('projects:project_materials', pk)
    update = False
    material = None
    if material_pk:
        material = Material.objects.get(pk=material_pk)
        instance = ProjectHasMaterial.objects.get(project=project,
                                                  material=material)
    else:
        instance = ProjectHasMaterial(project=project, add_to_report=True)

    form = AddMaterialToProjectForm(request.POST or None, instance=instance)

    if request.GET or not form.is_valid():
        if material:
            update = True
        
        return render(
            request,
            'projects/add_material_form.html',
            context={'form': form, 'project': project, 'material': material,
                     'update': update},
        )

    form.save()

    return redirect('projects:project_materials', pk)

@login_required
def remove_material_from_project(request, pk, material_pk):
    record = ProjectHasMaterial.objects.get(project=pk, material=material_pk)
    if record.project.owner == request.user:
        record.delete()
    return redirect('projects:project_materials', pk)

class ProjectDeleteView(LoginRequiredMixin, DeleteView):
    model = Project
    success_url = reverse_lazy('projects:list')

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        context['back_url'] = reverse_lazy('projects:list')
        return context
    
    def form_valid(self, request):
        self.object = self.get_object()
        # user = User.objects.get(username=self.request.user)
        if self.request.user == self.object.owner:
            success_url = self.get_success_url()
            self.object.delete()
            return HttpResponseRedirect(success_url)
        return render(self.request, 'materials/material_delete_forbidden.html',
                      context={'back_url': self.get_success_url()})


def add_create_material_to_project(request, pk):
    return HttpResponse(f'add_create_material_to_project for {pk}')
    # user = User.objects.get(username=request.user)
    # project = Project.objects.get(owner=user, pk=pk)
    # formset = formset_factory(AddMaterialForm, extra=2)
    # form_create_material = AddMaterialToProjectForm()
    # form_create_material.project = project
    # return render(request, 'projects/add_create_material_form.html',
    #               context={'form_create_material': formset, 
    #                        'form_add_material': form_create_material})


@login_required
def project_view_materials(request, pk):
    project = get_object_or_404(Project, pk=pk)
    param = request.GET.get('q')

    table = None
    obj = ProjectHasMaterial.objects.filter(project=project).filter(Q(project__owner=request.user) | Q(project__owner__is_staff=True))
    if param and request.user == project.owner:        
        obj = obj.filter(material__name__icontains=param)
    obj = obj.order_by('level', 'material__name')

    table = {
        'heads': ('Наименование', 'Уровень', 'Ед.изм.', '#action'),
        'lines': [
            {'data': (a.material.name, a.level, a.material.units),

             'link': '',
             'delete_link': reverse_lazy(
                  'projects:remove_material', kwargs={'pk': pk,
                                                      'material_pk': a.material.pk}
                    ),
             'edit_link': reverse_lazy('projects:mat-detail',
                                       kwargs={'pk': pk,
                                       'material_pk': a.material.pk}),}
            for a in obj
        ]
    }
    does_req_accept_json = request.accepts("application/json")
    is_ajax_request = request.headers.get('x-requested-with') == "XMLHttpRequest" and does_req_accept_json
    
    if is_ajax_request:

        html = render_to_string(
            template_name='includes/widgets/table.html',
            context={'table': table}
        )
        data_dict = {'html_from_view': html}
        return JsonResponse(data=data_dict, safe=False)

    context = {}
    context['table'] = table
    context['title'] = project.name
    context['pk'] = pk
    context['action_name'] = 'Добавить'
    context['action_url'] = reverse_lazy('projects:add_to_project',
                                            kwargs={'pk': pk})
    context['back_url'] = reverse_lazy('projects:detail',
                                           kwargs={'pk': pk})
    if param: context['back_url'] += '/?q' + param
    
    return render(request, 'projects/view_project_materials.html', context=context)
