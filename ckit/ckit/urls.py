from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import include, path
from django.views.generic.base import RedirectView, TemplateView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("auth/", include("users.urls")),
    path("auth/", include("django.contrib.auth.urls")),
    path("", include("core.urls")),
    path("units/", include("units.urls")),
    path("materials/", include("materials.urls")),
    path("projects/", include("projects.urls")),
    path("cities/", include("geo.urls")),
    path("calc_materials/", include("calc_materials.urls")),
    path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('favicon.ico'))),
    path('fish', TemplateView.as_view(template_name='includes/fish.html'), name='fish')
]

handler404 = "core.views.page_not_found"  # noqa
handler500 = "core.views.server_error"  # noqa
