from django.contrib import admin

from .models import Material, ProjectHasMaterial, MaterialGroup


class MaterialsAdmin(admin.ModelAdmin):
    list_display = ("pk", "name", "group", "owner")

class MaterialGroupAdmin(admin.ModelAdmin):
    list_display = ("pk", "name", 'owner')


class ProjectHasMaterialAdmin(admin.ModelAdmin):
    list_display = ("pk", "add_to_report", "level", "project", "material", "owner")

    def owner(self, instance):
        return instance.project.owner

    ordering = ["project__owner"]


admin.site.register(Material, MaterialsAdmin)
admin.site.register(MaterialGroup, MaterialGroupAdmin)
admin.site.register(ProjectHasMaterial, ProjectHasMaterialAdmin)
