from materials.models import ProjectHasMaterial
from rest_framework import serializers

from .models import Material


class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Material
        fields = ('name',)

class MaterialSerializerDetail(serializers.ModelSerializer):
    class Meta:
        model = ProjectHasMaterial
        fields = ('pk',)
