from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, viewsets

from .filters import MaterialFilter
from .models import Material
from .serializers import MaterialSerializer


class MaterialViewSet(generics.ListAPIView):
    queryset = Material.objects.all()#.order_by('name')
    serializer_class = MaterialSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']
