import django_filters

from .models import Material


class MaterialFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name',
                                     lookup_expr='istartswith')

    class Meta:
        model = Material
        fields = ('name',)
