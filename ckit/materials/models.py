from decimal import Decimal

from django.contrib.auth import get_user_model
from django.db import models
from projects.models import Project
from templates.messages import messages
from units.models import Unit

User = get_user_model()

class MaterialGroup(models.Model):
    name = models.CharField(max_length=40, verbose_name='Группа',
                            blank=False)
    
    owner = models.ForeignKey(User, on_delete=models.CASCADE,
                              related_name='group_materials',
                              verbose_name='Владелец')
    
    def __str__(self):
        return f'{self.name}({self.owner})'

    class Meta:
        ordering = ['name']
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'
        unique_together = [["name", "owner"]]


class Material(models.Model):
    name = models.CharField(
        max_length=50,
        unique=True,
        blank=False,
        verbose_name="Название",
        error_messages={"unique": messages["unique"]},
    )

    group = models.ForeignKey(MaterialGroup, blank=True,
                              null=True,
                              on_delete=models.DO_NOTHING,
                              related_name='materials')
    
    units = models.ForeignKey(
        Unit,
        blank=False,
        on_delete=models.CASCADE,
        related_name="material",
        to_field="name",
        verbose_name="Единицы измерения",
    )

    alt_units = models.ForeignKey(
        Unit,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="material_alt",
        to_field="name",
        verbose_name="Дополнительный единицы",
    )

    alt_to_units_ratio = models.DecimalField(
        verbose_name=("Коэффициент" " пересчета"),
        default=Decimal("1.0"),
        max_digits=19,
        decimal_places=10,
        null=False,
        blank=False,
    )
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="Владелец",
        related_name="materials",
    )

    def __str__(self):
        return f"{self.name}({self.owner})"

    class Meta:
        unique_together = [["name", "owner"]]
        verbose_name = "Материал"
        verbose_name_plural = "Материал"


class ProjectHasMaterial(models.Model):
    class Levels(models.IntegerChoices):
        ROOT = 0, '/'
        CHAPTER_1 = 1, 'Уровень 1'
        CHAPTER_2 = 2, 'Уровень 2'
        CHAPTER_3 = 3, 'Уровень 3'
        NODE = 4, 'Узел'
        LEAF = 5, 'Элемент'
    
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    material = models.ForeignKey(
        Material, on_delete=models.CASCADE, related_name="materials"
    )

    add_to_report = models.BooleanField(verbose_name="Добавить в отчет",
                                        default=True)
    level = models.IntegerField(choices=Levels.choices, default=Levels.NODE)

    def __str__(self):
        return f"{self.material.name}({self.project.owner}.{self.project})"

    class Meta:
        unique_together = [["project", "material"]]
        verbose_name = "Материалы в проектах"
        verbose_name_plural = verbose_name
