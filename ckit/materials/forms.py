from django.core.exceptions import NON_FIELD_ERRORS
from django.forms import ModelForm
from django.forms.widgets import HiddenInput, TextInput
from materials.models import Material


class AddMaterialForm(ModelForm):
    class Meta:
        model = Material
        fields = "__all__"
        widgets = {
            "owner": HiddenInput(),
            "name": TextInput(attrs={"autofocus": "autofocus"}),
        }

        error_messages = {
            NON_FIELD_ERRORS: {"unique_together": f"Уже существует"}
        }

    def __init__(self, *args, **kwargs):
        initial = kwargs.get("initial", {})
        initial.update({"owner": kwargs.pop("user")})
        kwargs.update({"initial": initial})
        super(AddMaterialForm, self).__init__(*args, **kwargs)
