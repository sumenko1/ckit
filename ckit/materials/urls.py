from django.urls import include, path, re_path
from django.views.decorators.csrf import csrf_exempt
from rest_framework import routers

from .views import (
    MaterialCreateView, MaterialDeleteView, material_detail,
    search_material, get_list_by_group
)
from .views_api import MaterialViewSet

app_name = "materials"

router = routers.DefaultRouter()

urlpatterns = [
    path("", search_material, name="list"),
    path("new/", MaterialCreateView.as_view(), name="new"),
    path("rm/<int:pk>/", MaterialDeleteView.as_view(), name="remove"),
    path("detail/<int:pk>/<int:pk_material>/",material_detail, name="api_detail"),
    path("s/", search_material, name="search"),
    path("list/", get_list_by_group, name="by_group")
]
