from typing import Any, Dict


from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import (
    HttpResponse, HttpResponseForbidden, HttpResponseRedirect, JsonResponse,
)
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView
from projects.models import Project

from .forms import AddMaterialForm
from .models import Material, ProjectHasMaterial, MaterialGroup
from .serializers import MaterialSerializerDetail


User = get_user_model()


class MaterialCreateView(LoginRequiredMixin, CreateView):
    form_class = AddMaterialForm
    template_name = "materials/material_form.html"
    success_url = reverse_lazy("materials:list")
    login_url = reverse_lazy("login")

    def get_form_kwargs(self) -> dict[str, Any]:
        # Put user to kw for form class
        kwargs = super().get_form_kwargs()
        kwargs.update({"user": self.request.user})
        return kwargs



class MaterialDeleteView(LoginRequiredMixin, DeleteView):
    model = Material
    success_url = reverse_lazy("materials:list")

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["back_url"] = reverse_lazy("materials:list")
        return context
    
    def form_valid(self, request, *args, **kwargs):
        self.object = self.get_object()
        if (self.request.user.is_superuser or
            self.request.user == self.object.owner):
            success_url = self.get_success_url()
            self.object.delete()
            return HttpResponseRedirect(success_url)
        return render(self.request, 'materials/material_delete_forbidden.html',
                      context={'back_url': self.get_success_url()})


def material_detail(request, pk, pk_material):
    project = get_object_or_404(Project, pk=pk)
    if project.owner != request.user:
        return HttpResponseForbidden('You must be an owner of the project')
    material = ProjectHasMaterial.objects.filter(project=project,
                                                 pk=pk_material)
    
    s = MaterialSerializerDetail(material).data
    print(s)
    if not material:
        return HttpResponse((f'{pk_material} not found in {project}'), status=404)
    return HttpResponse(s)


def search_material(request):
    param = request.GET.get('q')
    if param:
        materials = Material.objects.filter(name__icontains=param).filter(
                                            Q(owner=request.user) | Q(owner__is_staff=True))
    else:
        materials = Material.objects.all()
    materials = materials.order_by('group', 'name')
    table = {
        'heads': (),
        'lines': [
            {'data': (a.name, a.group, a.units),
                'link': '#'}
            for a in materials
        ]
    }
    does_req_accept_json = request.accepts("application/json")
    is_ajax_request = request.headers.get('x-requested-with') == "XMLHttpRequest" and does_req_accept_json

    if is_ajax_request:

        html = render_to_string(
            template_name='includes/widgets/table.html',
            context={'table': table}
        )
        data_dict = {'html_from_view': html}
        return JsonResponse(data=data_dict, safe=False)
    
    return render(request, 'materials/view_materials_list.html', context={'table': table})

@login_required
def get_list_by_group(request):
    
    gid_str = request.GET.get('gid', None)
    try:
        group_id = int(gid_str)
    except (TypeError, ValueError):
        group_id = None

    groups = MaterialGroup.objects.filter(Q(owner=request.user) | Q(owner__is_staff=True))
    if group_id:
        materials = Material.objects.filter((Q(owner=request.user) | Q(owner__is_staff=True)) & Q(group__pk=group_id))
    else:
        materials = Material.objects.filter((Q(owner=request.user) | Q(owner__is_staff=True)) & Q(group__pk=None))
    context ={
        'groups': groups,
        'materials': materials,
        'selected': group_id
    }
    does_req_accept_json = request.accepts("application/json")
    is_ajax_request = request.headers.get('x-requested-with') == "XMLHttpRequest" and does_req_accept_json

    if is_ajax_request:
        html = render_to_string(
            template_name='materials/widgets/material_list.html',
            context={'materials': materials}
        )
        data_dict = {'html_from_view': html}
        return HttpResponse(html)

    return render(request, 'materials/widgets/material_selector.html', context=context)
