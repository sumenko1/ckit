from django.contrib import admin

from .models import City, CityData, RegionSnow, RegionWind, StandartName


class CitiesAdmin(admin.ModelAdmin):
    list_display = ("name",)


class StandartNameAdmin(admin.ModelAdmin):
    list_display = ("name",)


class RegionSnowAdmin(admin.ModelAdmin):
    list_display = (
        "standart",
        "region_snow",
    )


class RegionWindAdmin(admin.ModelAdmin):
    list_display = (
        "standart",
        "region_wind",
    )


class CityDataAdmin(admin.ModelAdmin):
    list_display = ("city", "standart", "snow", "wind")


admin.site.register(City, CitiesAdmin)
admin.site.register(StandartName, StandartNameAdmin)
admin.site.register(CityData, CityDataAdmin)
admin.site.register(RegionSnow, RegionSnowAdmin)
admin.site.register(RegionWind, RegionWindAdmin)
