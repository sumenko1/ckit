from django.urls import path

from .views import CityCreateView, CityDeleteView, CityListView

app_name = "geo"

urlpatterns = [
    path("", CityListView.as_view(), name="list"),
    path("new/", CityCreateView.as_view(), name="new"),
    path("rm/<int:pk>/", CityDeleteView.as_view(), name="remove"),
]
