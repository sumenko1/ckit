from typing import Any, Dict

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, DeleteView

from .forms import CityForm
from .models import City


class CityCreateView(LoginRequiredMixin, CreateView):
    model = City
    form_class = CityForm
    success_url = reverse_lazy("geo:list")
    login_url = reverse_lazy("login")


class CityListView(TemplateView):
    template_name = "geo/cities.html"

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["table"] = {
            "counter": True,
            "heads": ("Город", "#action"),
            "lines": [
                {
                    "data": [city],
                    "link": f"link_to { city.pk }",
                    "delete_link": reverse_lazy(
                        "geo:remove", kwargs={"pk": city.pk}
                    ),
                }
                for city in City.objects.all()
            ],
        }
        return context


class CityDeleteView(LoginRequiredMixin, DeleteView):
    model = City
    success_url = reverse_lazy("geo:view")

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["back_url"] = reverse_lazy("geo:view")
        return context
