from django.db import models
from roman import fromRoman, toRoman


class City(models.Model):
    name = models.CharField(
        verbose_name="Название", max_length=30, blank=False, unique=True
    )

    class Meta:
        ordering = ["name"]
        verbose_name = "Город"
        verbose_name_plural = "Города"

    def __str__(self):
        return self.name
    
    @property
    def get_name(self):
        if self.name:
              return self.name
        else:
              return "<!>"

class StandartName(models.Model):
    name = models.CharField(
        max_length=50, verbose_name="Название", blank=False, unique=True
    )

    class Meta:
        ordering = ["name"]
        verbose_name = "Нормативный документ"
        verbose_name_plural = "Нормативный документы"

    def __str__(self):
        return self.name


class RegionSnow(models.Model):
    SNOW_REGION_CHOICES = [(i, toRoman(i)) for i in range(1, 8 + 1)]
    standart = models.ForeignKey(
        StandartName, on_delete=models.CASCADE, blank=False
    )
    region_snow = models.SmallIntegerField(
        verbose_name="Снеговой район",
        choices=SNOW_REGION_CHOICES,
        default="I",
        blank=False,
    )

    class Meta:
        verbose_name = "Снеговой район"
        verbose_name_plural = "Снеговые районы"
        unique_together = [["standart", "region_snow"]]

    def __str__(self):
        return toRoman(self.region_snow)


class RegionWind(models.Model):
    WIND_REGION_CHOICES = [(i, toRoman(i)) for i in range(1, 7 + 1)]
    standart = models.ForeignKey(
        StandartName, on_delete=models.CASCADE, blank=False
    )
    region_wind = models.SmallIntegerField(
        verbose_name="Ветровой район",
        choices=WIND_REGION_CHOICES,
        default="I",
        blank=False,
    )

    class Meta:
        verbose_name = "Ветровой район"
        verbose_name_plural = "Ветровые районы"
        unique_together = [["standart", "region_wind"]]

    def __str__(self):
        return toRoman(self.region_wind)


class CityData(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE, blank=False)
    standart = models.ForeignKey(
        StandartName, on_delete=models.CASCADE, blank=False
    )
    snow = models.ForeignKey(RegionSnow, on_delete=models.CASCADE, blank=False)
    wind = models.ForeignKey(RegionWind, on_delete=models.CASCADE, blank=False)

    class Meta:
        verbose_name = "Данные города"
        verbose_name_plural = "Данные городов"
        unique_together = [["city", "standart"]]
