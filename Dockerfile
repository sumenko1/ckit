FROM python:3.10.4
LABEL author='Sumenko V' version='1'
RUN mkdir /code

COPY requirements.txt /code

RUN pip3 install -r /code/requirements.txt

COPY ckit /code

WORKDIR /code
CMD gunicorn ckit.wsgi:application --bind 0.0.0.0:8123
